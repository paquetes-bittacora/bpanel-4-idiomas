# Idiomas

Paquete para gestionar los idiomas de los que va a constar la parte pública de la web.  
Consiste en un listado los cuales se componen de:

1. Nombre (no puede repetirse)
2. Locale (https://es.wikipedia.org/wiki/ISO_639-1)
3. Region (https://es.wikipedia.org/wiki/ISO_3166-1)
4. Activo (Siempre tiene que haber un idioma activo)
5. Principal (Siempre tiene que haber un idioma principal)
6. Orden

## LanguageFacade

Tiene dos funciones suficientes para obtener información de los idiomas:
1. Language::getActives()   - Obtiene los idiomas activos en un collection de LanguageModel ordenados por orden ascendente
2. Language::getDefault()  - Obtiene el idioma principal -> LanguageModel

# Instalación
```
php artisan vendor:publish --tag=language
```
Añadir a webpack.mix.js

```js
// bittacora/bpanel4-language  
.sass('resources/language/scss/language.scss', 'public/css')
```
