{{-- Pinta las banderas de los idiomas para crear o editar un contenido en función de las traducciones que tenga
     guardadas.  --}}
@foreach(Bittacora\Language\Language::getActives() as $language)
    @if(in_array($language->locale, Bittacora\Language\Language::getModelLanguages($model), true))
        <a href="{{ route($edit_route_name, [$model, 'locale' => $language->locale]) }}">
            <img src="{{asset('img/flags/'.$language->locale.'.png')}}"
                 alt="Editar el contenido en {{ $language->name }}"
                 title="Editar el contenido en {{ $language->name }}">
        </a>
    @else
        <a href="{{ route($edit_route_name, [$model, 'locale' => $language->locale]) }}"><img src="{{asset('img/flags/'.$language->locale.'.png')}}" style="opacity: 0.3; filter: saturate(0);"
             alt="Crear el contenido en {{ $language->name }}"
             title="Crear el contenido en {{ $language->name }}"></a>
    @endif
@endforeach
