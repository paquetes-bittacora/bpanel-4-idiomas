<style>
    .content-language-icon {
        position: relative;
    }

    .content-language-icon::before {
        display: none;

        font-family: "Font Awesome 5 Pro";
        font-weight: 900;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        text-align: center;
        color: white;
        text-shadow: 1px 1px black, -1px 1px black, -1px -1px black, 1px -1px black;
    }

    .content-language-icon-edit::before {
        content: "\f303";
    }

    .content-language-icon-create::before {
        content: "\f067";
    }
</style>

{{-- Pinta las banderas de los idiomas para crear o editar un contenido en función de las traducciones que tenga
     guardadas.  --}}

<div class="d-flex justify-content-end">
    @foreach($languages as $language)
        @if(in_array($language->locale, Bittacora\Language\Language::getModelLanguages($model), true))
            {{-- El contenido ya está creado en este idioma --}}
            <a href="{{ route($edit_route_name, [
                'model' => $model,
                'locale' => $language->locale,
                ...($additionalEditRouteParameters ?? [])
                ]) }}" class="ml-1 content-language-icon content-language-icon-edit">
                <img src="{{asset('img/flags/'.$language->locale.'.png')}}"
                     alt="Editar el contenido en {{ $language->name }}"
                     title="Editar el contenido en {{ $language->name }}"
                     @if($language->locale !== $currentLanguage) style="opacity: 0.3; filter: saturate(0);" @endif>
            </a>
        @else
            {{-- El contenido no existe en este idioma --}}
            <a href="{{ route($edit_route_name, [
                'model' => $model,
                'locale' => $language->locale,
                ...($additionalEditRouteParameters ?? [])
                ]) }}" class="ml-1 content-language-icon content-language-icon-create">
                <img src="{{asset('img/flags/'.$language->locale.'.png')}}"
                     alt="Crear el contenido en {{ $language->name }}"
                     title="Crear el contenido en {{ $language->name }}"
                     @if($language->locale !== $currentLanguage) style="opacity: 0.3; filter: saturate(0);" @endif>
            </a>
        @endif
    @endforeach
</div>
