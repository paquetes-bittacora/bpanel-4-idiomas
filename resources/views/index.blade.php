@extends('bpanel4::layouts.bpanel-app')

@pushOnce('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/language.css') }}">
@endpushOnce

@section('title', 'Idiomas')

@inject('category', 'Bittacora\Category\Category')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('language::language.language_list') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('language::language-datatable', [], key('language-datatable'))
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ Vite::asset('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js') }}"></script>
@endpush
