<div class="text-center">
    @if ($value==0)
        <button class="btn btn-danger btn-sm mb-1" wire:click="toggle({{$idField}})">
            <i class="fa fa-times"></i>
        </button>
    @else
        <button class="btn btn-success btn-sm mb-1" wire:click="toggle({{$idField}})">
            <i class="fa fa-check"></i>
        </button>
    @endif
</div>
