{{-- Pinta las banderas de los idiomas para crear o editar un contenido en función de las traducciones que tenga
     guardadas.  --}}
<div class="text-center">
    @foreach(\Bittacora\Language\LanguageFacade::getActives() as $language)
        @if(in_array($language->locale, Bittacora\Language\Language::getModelLanguages($model), true))
            <a href="{{ route($editRouteName, [$model, 'locale' => $language->locale, ...$additionalEditRouteParameters]) }}"><img src="{{asset('img/flags/'.$language->locale.'.png')}}"
                                                                                                alt="Editar el contenido en {{ $language->name }}"
                                                                                                title="Editar el contenido en {{ $language->name }}"></a>
        @else
            <a href="{{ route($editRouteName, [$model, 'locale' => $language->locale,...$additionalCreateRouteParameters ]) }}"><img src="{{asset('img/flags/'.$language->locale.'.png')}}" style="opacity: 0.3; filter: saturate(0);"
                                                                                                  alt="Crear el contenido en {{ $language->name }}"
                                                                                                  title="Crear el contenido en {{ $language->name }}"></a>
        @endif
    @endforeach
</div>
