<td>
    <div class="text-center">
        {{$row->name}}
    </div>
</td>
<td>
    <div class="text-center">
        {{$row->locale}}
    </div>
</td>
<td>
    <div class="text-center">
        {{$row->region}}
    </div>
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-language-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'default', 'model' => $row, 'value' => $row->default, 'size' => 'xxs', 'reload' => 'language.index'], key('default-language-'.$row->id))
</td>
<td>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'language', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el idioma?'], key('language-buttons-'.$row->id))
    </div>
</td>
