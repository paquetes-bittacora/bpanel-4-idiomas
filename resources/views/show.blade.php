@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Roles')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 mb-5">
            <h4 class="text-120">
                <span class="text-90">{{ __('language::language.show') }}</span>
            </h4>
        </div>

            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('language::language.name'), 'required'=>true,'disabled' => true, 'value'=>$language->name])
            @livewire('form::input-text', ['name' => 'locale', 'labelText' => __('language::language.locale'), 'required'=>true,'disabled' => true,
        'value'=>$language->locale])
            @livewire('form::input-text', ['name' => 'locale', 'labelText' => __('language::language.region'), 'required'=>true,'disabled' => true,
        'value'=>$language->region])

            @livewire('form::input-checkbox', ['name' => 'active', 'labelText' => __('language::language.active'), 'bpanelForm' => true,'disabled' => true, 'checked' =>
        $language->active])
            @livewire('form::input-checkbox', ['name' => 'default', 'labelText' => __('language::language.default'), 'bpanelForm' => true,'disabled' => true, 'checked' =>
        $language->default])

        @livewire('utils::created-updated-info', ['model' => $language])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                <a class="btn btn-success btn-bold px-4 mx-2" href="{{route('language.index')}}">
                    <i class="fa fa-list mr-1" aria-hidden="true"></i>
                    &nbsp;Listado
                </a>
            </div>
        </form>
    </div>
@endsection
