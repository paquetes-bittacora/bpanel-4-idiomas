@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Idiomas')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120">
                <span class="text-90">{{ __('language::language.add') }}</span>
            </h4>
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('language.store')}}">

            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('language::language.name'), 'required'=>true])
            @livewire('form::select', [
            'name' => 'locale',
            'labelText' => __('language::language.locale'),
            'required' => true,
            'allValues' => $locales
            ])
            @livewire('form::select', [
            'name' => 'region',
            'labelText' => __('language::language.region'),
            'required' => true,
            'allValues' => $regions
            ])
            @livewire('form::input-checkbox', ['name' => 'active', 'labelText' => __('language::language.active'), 'bpanelForm' => true])
            @livewire('form::input-checkbox', ['name' => 'default', 'labelText' => __('language::language.default'), 'bpanelForm' => true])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>
@endsection
