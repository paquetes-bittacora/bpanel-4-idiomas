<?php

return[
    'index' => 'Listar idiomas',
    'create' => 'Crear idioma',
    'show' => 'Mostrar idioma',
    'edit' => 'Editar idioma',
    'delete' => 'Borrar idioma',
    'add' => 'Crear nuevo idioma',
    'language_name' => 'Nombre',
    'language_list' => 'Listado de idiomas',
    'language_editing' => 'Editando idioma',
    'locale' => 'Locale',
    'region' => 'Region',
    'default' => 'Principal',
    'active' => 'Activo',
    'name' => 'Nombre',
    'destroy' => 'Eliminar idioma'
];
