<?php

namespace Bittacora\Language\Database\Seeders\seeds;

use App\Models\User;
use Bittacora\Language\Models\LanguageModel;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where(['email' => 'debug@bpanel.es'])->firstOrFail();

        $language = LanguageModel::create([
            'name' => 'Español',
            'locale' => 'es',
            'region' => 'ES',
            'active' => 1,
            'default' => 1,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);
        $language->save();

        $language = LanguageModel::create([
            'name' => 'Inglés',
            'locale' => 'en',
            'region' => 'GB',
            'active' => 0,
            'default' => 0,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);
        $language->save();

        $language = LanguageModel::create([
            'name' => 'Portugués',
            'locale' => 'pt',
            'region' => 'PT',
            'active' => 0,
            'default' => 0,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);
        $language->save();

        $language = LanguageModel::create([
            'name' => 'Francés',
            'locale' => 'fr',
            'region' => 'FR',
            'active' => 0,
            'default' => 0,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);
        $language->save();

        $language = LanguageModel::create([
            'name' => 'Alemán',
            'locale' => 'de',
            'region' => 'DE',
            'active' => 0,
            'default' => 0,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);
        $language->save();
    }
}
