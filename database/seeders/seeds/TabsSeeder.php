<?php

namespace Bittacora\Language\Database\Seeders\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Bittacora\Tabs\Tabs;

class TabsSeeder extends Seeder
{
    public function run()
    {
        Tabs::createItem('language.index', 'language.index', 'language.index', 'Idiomas','fa fa-list');
        Tabs::createItem('language.index', 'language.create', 'language.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('language.create', 'language.index', 'language.index', 'Listado','fa fa-list');
        Tabs::createItem('language.create', 'language.create', 'language.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('language.show', 'language.index', 'language.index', 'Idiomas','fa fa-list');
        Tabs::createItem('language.show', 'language.create', 'language.create', 'Nuevo','fa fa-plus');
        Tabs::createItem('language.show', 'language.show', 'language.show', 'Ver','fa fa-eye');
        Tabs::createItem('language.show', 'language.edit', 'language.edit', 'Editar','fa fa-edit');

        Tabs::createItem('language.edit', 'language.index', 'language.index', 'Idiomas','fa fa-list');
        Tabs::createItem('language.edit', 'language.create', 'language.create', 'Nuevo','fa fa-plus');
        Tabs::createItem('language.edit', 'language.show', 'language.show', 'Ver','fa fa-eye');
        Tabs::createItem('language.edit', 'language.edit', 'language.edit', 'Editar','fa fa-edit');
    }
}
