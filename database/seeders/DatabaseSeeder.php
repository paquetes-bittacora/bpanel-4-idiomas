<?php

namespace Bittacora\Language\Database\Seeders;

use Bittacora\Language\Database\Seeders\seeds\LanguagePermissionSeeder;
use Bittacora\Language\Database\Seeders\seeds\LanguageSeeder;
use Bittacora\Language\Database\Seeders\seeds\LocaleSeeder;
use Bittacora\Language\Database\Seeders\seeds\RegionSeeder;
use Bittacora\Language\Database\Seeders\seeds\TabsSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RegionSeeder::class,
            LocaleSeeder::class,
            LanguageSeeder::class,
            LanguagePermissionSeeder::class,
            TabsSeeder::class
        ]);
    }
}
