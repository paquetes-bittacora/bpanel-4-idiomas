<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable(false);
            $table->char('locale', 2)->nullable(false);
            $table->char('region', 2)->nullable(false);
            $table->tinyInteger('active')->nullable(false)->default(0);
            $table->tinyInteger('default')->nullable(false)->default(0);
            $table->tinyInteger('order_column')->nullable(false)->unsigned();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('locale')->references('code')->on('languages_locales');
            $table->foreign('region')->references('alfa_2')->on('languages_regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('languages');
        Schema::enableForeignKeyConstraints();
    }
}
