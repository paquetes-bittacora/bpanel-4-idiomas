<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages_regions', function (Blueprint $table) {
            $table->string('name', 100);
            $table->string('iso', 150);
            $table->char('alfa_2', 2);
            $table->char('alfa_3', 3);
            $table->char('code', 3);
            $table->primary('code');
            $table->unique('alfa_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('languages_regions');
        Schema::enableForeignKeyConstraints();
    }
}
