<?php

use Bittacora\Language\Http\Controllers\LanguageController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('language.')->group(function(){
    Route::get('language', [LanguageController::class, 'index'])->name('index');
    Route::get('language/create', [LanguageController::class, 'create'])->name('create');
    Route::get('language/{language}', [LanguageController::class, 'show'])->name('show');
    Route::post('language/store', [LanguageController::class, 'store'])->name('store');
    Route::get('language/{language}/edit', [LanguageController::class, 'edit'])->name('edit');
    Route::put('language/{language}', [LanguageController::class, 'update'])->name('update');
    Route::delete('language/{language}', [LanguageController::class, 'destroy'])->name('destroy');
    Route::post('language/reorder', [LanguageController::class, 'reorder'])->name('reorder');
});
