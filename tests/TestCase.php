<?php

declare(strict_types=1);


namespace Bittacora\Language\Tests;

use Bittacora\Language\LanguageServiceProvider;
use Livewire\LivewireServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            LanguageServiceProvider::class,
            LivewireServiceProvider::class,
        ];
    }

    /**
     * Set up the environment.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function defineEnvironment($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        $app['config']->set('view.paths', [__DIR__.'/resources/views']);

        dd("sadasd");
//
//        // Set-up admin guard
//        $app['config']->set('auth.guards.admin', ['driver' => 'session', 'provider' => 'admins']);
//        $app['config']->set('auth.providers.admins', ['driver' => 'eloquent', 'model' => Admin::class]);
//
//        // Use test User model for users provider
//        $app['config']->set('auth.providers.users.model', User::class);
//
//        $app['config']->set('cache.prefix', 'spatie_tests---');
    }

}
