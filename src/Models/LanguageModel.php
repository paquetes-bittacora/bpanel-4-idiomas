<?php

declare(strict_types=1);

namespace Bittacora\Language\Models;

use Bittacora\Language\Observers\LanguageObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class LanguageModel extends Model implements Sortable
{
    use SortableTrait;

    public $fillable = ['name','locale', 'region', 'active','default','created_user','updated_user'];
    protected $table = 'languages';

    protected $casts = [
        'active' => 'integer',
        'default' => 'integer',
    ];

    protected $dispatchesEvents = [
        'creating' => LanguageObserver::class,
        'updating' => LanguageObserver::class,
        'deleting' => LanguageObserver::class,
    ];

    public $sortable = [
        'order_column_name' => 'order_column',
        'sort_when_creating' => true,
    ];

    public function locale()
    {
        return $this->hasOne(Locale::class);
    }

    public function region()
    {
        return $this->hasOne(Region::class);
    }

    /**
     * Scope a query to only include active language.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1)->orderBy('order_column');
    }

    /**
     * Scope a query to only include active language.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDefault(Builder $query): Builder
    {
        return $query->where('default', 1);
    }


}
