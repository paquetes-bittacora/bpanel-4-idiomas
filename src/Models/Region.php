<?php

declare(strict_types=1);

namespace Bittacora\Language\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $timestamps = false;
    protected $table = 'languages_regions';
    public $fillable = ['name','iso', 'alfa_2', 'alfa_3','code'];
}
