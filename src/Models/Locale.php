<?php

declare(strict_types=1);

namespace Bittacora\Language\Models;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    public $timestamps = false;
    protected $table = 'languages_locales';
    public $fillable = ['name','code'];
}
