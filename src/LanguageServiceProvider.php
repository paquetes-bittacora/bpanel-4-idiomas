<?php

namespace Bittacora\Language;

use Bittacora\Language\Commands\InstallCommand;
use Bittacora\Language\DataTables\LanguageDatatable;
use Bittacora\Language\Http\Livewire\DatatableLanguages;
use Bittacora\Language\Models\LanguageModel;
use Bittacora\Language\Observers\LanguageObserver;
use Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class LanguageServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('language')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_languages_locales')
            ->hasMigration('create_languages_regions')
            ->hasMigration('create_languages_table');
    }

    public function register(){
        $this->app->bind('language', function($app){
            return new Language();
        });
    }

    public function boot(){
        LanguageModel::observe(LanguageObserver::class);

        $this->commands([InstallCommand::class]);      

        $this->app->register(SeedServiceProvider::class);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__. '/../resources/views', 'language');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'language');
        $this->publishes([
            __DIR__.'/../resources/js' => public_path('vendor/language/js'),
            __DIR__.'/../resources/scss' => resource_path('language/scss'),
            __DIR__.'/../resources/img/flags' => public_path('img/flags/'),
        ], 'language');

        Livewire::component('language::datatable-languages', DatatableLanguages::class);
        Livewire::component('language::language-datatable', Http\Livewire\LanguageDatatable::class);


    }

}
