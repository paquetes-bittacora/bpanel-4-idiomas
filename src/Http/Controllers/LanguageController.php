<?php

namespace Bittacora\Language\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Language\DataTables\LanguageDatatable;
use Bittacora\Language\Http\Requests\StoreLanguageRequest;
use Bittacora\Language\Http\Requests\UpdateLanguageRequest;
use Bittacora\Language\Models\LanguageModel;
use Bittacora\Language\Models\Locale;
use Bittacora\Language\Models\Region;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Utilities\Request;

class LanguageController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('language.index');
        return view('language::index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('language.create');
        return view('language::create')->with([
            'locales' => Locale::all()->pluck('name', 'code')->toArray(),
            'regions' => Region::all()->pluck('name', 'alfa_2')->toArray()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLanguageRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreLanguageRequest $request): RedirectResponse
    {
        $this->authorize('language.create');
        $language = new LanguageModel($request->all());
        $language->save();

        Session::put('alert-success', 'Idioma creado correctamente');
        return redirect(route('language.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param LanguageModel $language
     * @return View
     * @throws AuthorizationException
     */
    public function show(LanguageModel $language): View
    {
        $this->authorize('language.show');
        return view('language::show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param LanguageModel $language
     * @return View
     * @throws AuthorizationException
     */
    public function edit(LanguageModel $language): View
    {
        $this->authorize('language.edit');
        return view('language::edit')->with([
            'locales' => Locale::all()->pluck('name', 'code')->toArray(),
            'regions' => Region::all()->pluck('name', 'alfa_2')->toArray(),
            'language' => $language
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLanguageRequest $request
     * @param LanguageModel $language
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateLanguageRequest $request, LanguageModel $language): RedirectResponse
    {
        $this->authorize('language.edit');
        $language->update($request->all());
        Session::put('alert-success', 'Idioma editado correctamente');
        return redirect(route('language.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param LanguageModel $language
     * @throws AuthorizationException
     */
    public function destroy(LanguageModel $language)
    {
        $this->authorize('language.destroy');
        if ($language->delete()){
            Session::put('alert-success', 'Idioma borrado correctamente');
        }else{
            Session::put('alert-warning', 'No se puede borrar este idioma');
        }
        return redirect(route('language.index'));
    }

    /**
     * @param Request $request
     */
    public function reorder(Request $request)
    {
        $collect=collect($request->json()->all());
        LanguageModel::setNewOrder($collect->pluck('id'),$collect->first()['position']);
    }
}
