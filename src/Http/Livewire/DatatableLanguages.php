<?php

namespace Bittacora\Language\Http\Livewire;

use Livewire\Component;

class DatatableLanguages extends Component
{
    public $model;
    public $createRouteName;
    public $editRouteName;
    public $additionalEditRouteParameters = [];
    public $additionalCreateRouteParameters = [];

    public function render()
    {
        return view('language::livewire.datatable-languages');
    }
}
