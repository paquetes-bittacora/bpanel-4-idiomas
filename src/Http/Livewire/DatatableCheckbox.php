<?php

namespace Bittacora\Language\Http\Livewire;

use Bittacora\Language\Models\LanguageModel;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $language;

    public function mount(LanguageModel $language){
        $this->language = $language;
    }

    public function render()
    {
        return view('language::livewire.datatable-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(LanguageModel $languageModel){
        $languageModel->update(['active' => !$languageModel->active]);
        $this->checked = $languageModel->active;
    }
}
