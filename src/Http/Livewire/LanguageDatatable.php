<?php

namespace Bittacora\Language\Http\Livewire;

use Bittacora\Language\Models\LanguageModel;
use Illuminate\Database\Eloquent\Builder;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class LanguageDatatable extends DataTableComponent
{
    use LivewireAlert;

    public ?string $defaultSortColumn = 'order_column';
    public string $defaultSortDirection = 'asc';

    public function columns(): array
    {
        return [
            Column::make('Título', 'name')->sortable(),
            Column::make('Locale', 'locale')->sortable(),
            Column::make('Región', 'region')->sortable(),
            Column::make('Activo', 'active')->view('language::livewire.datatable-columns.active-column'),
            Column::make('Principal', 'default')->sortable()->view('language::livewire.datatable-columns.default-column'),
            Column::make('Orden', 'order_column')->sortable()->view('language::livewire.datatable-columns.order-column'),
            Column::make('Acciones', 'id')->view('language::livewire.datatable-columns.actions-column'),
        ];
    }

    public function query(): Builder
    {
        return LanguageModel::query()->
        when($this->getAppliedFilterWithValue('search'), fn ($query, $term) => $query->where('name', 'like', '%'.strtoupper($term).'%')
            ->orWhere('name', 'like', '%'.strtolower($term).'%')->orWhere('name', 'like', '%'.ucfirst($term).'%'));
    }

    public function reorder($list){
        foreach($list as $item){
            LanguageModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            foreach($this->selectedKeys() as $key){
                if(!LanguageModel::where('id', $key)->where('default',1)->count()){
                    LanguageModel::destroy($key);
                }else{
                    $this->alert('error', 'No se puede eliminar el idioma principal.');
                }
            }

            $ids = LanguageModel::ordered()->pluck('id');
            LanguageModel::setNewOrder($ids);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setDefaultReorderSort('order_column', 'asc');
        $this->setReorderEnabled();
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
