<?php

namespace Bittacora\Language\Http\Livewire;

use Bittacora\Language\Models\LanguageModel;
use Livewire\Component;

class DatatableDefault extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $language;

    public function mount(LanguageModel $language){
        $this->language = $language;
    }

    public function render()
    {
        return view('language::livewire.datatable-default')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'value' => $this->value,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(LanguageModel $languageModel){
        $languageModel->update(['default' => !$languageModel->default]);
        $this->value = $languageModel->default;
        return redirect()->to(route('language.index'));
    }
}
