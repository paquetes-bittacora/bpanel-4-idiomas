<?php

namespace Bittacora\Language\Http\Requests;

use Bittacora\Language\Models\Locale;
use Bittacora\Language\Models\Region;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateLanguageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('languages')
                    ->where('locale', $this->locale)
                    ->where('region', $this->region)
                    ->where('name', $this->name)
                    ->ignore($this->id)
            ],
            'locale' => [
                'required',
                'min:2',
                'max:2',
                Rule::in(Locale::all()->pluck('code')->toArray())
            ],
            'region' => [
                'required',
                'min:2',
                'max:2',
                Rule::in(Region::all()->pluck('alfa_2')->toArray())
            ],
            'active' => "exclude_unless:default,true|required|accepted",
        ];
    }

    public function messages(){
        return [
            'active.accepted' => "Es obligatorio marcar el idioma como activado si va a ser principal",
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'active' => isset($this->active),
            'default' => isset($this->default),
        ]);
    }
}
