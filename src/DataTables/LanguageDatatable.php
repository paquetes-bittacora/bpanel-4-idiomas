<?php

namespace Bittacora\Language\DataTables;

use Bittacora\Language\Models\LanguageModel;
use Bittacora\Utils\DataTable\Reorder;
use Livewire\ComponentConcerns\ReceivesEvents;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Options\Plugins\RowReorder;
use Yajra\DataTables\Services\DataTable;

class LanguageDatatable extends DataTable
{
    use RowReorder, Reorder;

    protected array $actions = [];

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('active', function(LanguageModel $language){
                    return view ('utils::partials.datatable-checkbox')->with([
                        'model' => $language,
                        'fieldName' => 'active',
                        'permission' => 'language.edit',

                    ]);
            })
            ->addColumn('default', function(LanguageModel $language){
                return view ('utils::partials.datatable-default')->with([
                    'model' => $language,
                    'fieldName' => 'default',
                    'reload' => 'language.index', //route
                    'permission' => 'language.edit',
                    'size' => 'xxs'
                ]);
            })
            ->addColumn('order_column', function ($query) {
                return view ('utils::partials.datatable-order')->with([
                    'order_column' => $query->order_column,
                    'permission' => 'language.edit'
                ]);
            })
            ->addColumn('action', function (LanguageModel $language) {
                return view('utils::partials.datatable-button')->with([
                    'buttons' => [
                        'show', 'edit', 'destroy'
                    ],
                    'model' => $language,
                    'scope' => 'language'
                ]);
            });
    }

    public function query(LanguageModel $languageModel)
    {
        return $languageModel->newQuery()->orderBy('order_column');
    }

    public function html()
    {
        return $this->builder()
            ->rowReorder([
                'update' => true, //Actualiza la tabla una vez se ha completado el reorder
                'selector' => 'td:nth-child(6)' //Nº de columna que va a ser la que reordene
            ])
            ->rowId('id') //Necesario para el reorder
            ->addTableClass('table text-dark-m2 text-95 bgc-white ml-n1px')
            ->columns($this->getColumns())
            ->parameters([
                'dom' => 'Bfrtip',
                'buttons' => $this->actions
            ])
            ->paging(true)
            ->minifiedAjax()
            ->drawCallbackWithLivewire()
            ->dom("<'row'<'col-3'l><'col-9'f>><'row'<'col-12't>><'row'<'col-12'p>><'clear'>")
            ->language('/vendor/datatables/spanish.json');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->title('Título')
                ->addClass('d-flex flex-row align-items-center justify-content-center  ')
                ->orderable(false),
            Column::make('locale')->title('Locale')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center')
                ->orderable(false),
            Column::make('region')->title('Region')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center')
                ->orderable(false),
            Column::computed('active')->title('Activo')
                ->printable(false)
                ->exportable(true)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
            Column::computed('default')->title('Principal')
                ->searchable(false)
                ->printable(false)
                ->exportable(true)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
            Column::computed('order_column')->title('Orden')
                ->searchable(false)
                ->printable(false)
                ->exportable(true)
                ->orderable(true)
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center cursor'),
            Column::computed('action')->title('Acciones')
                ->printable(false)
                ->exportable(false)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Idiomas_' . date('YmdHis');
    }
}
