<?php

namespace Bittacora\Language;

use Bittacora\Language\Models\LanguageModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Spatie\Permission\Models\Permission;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;

class Language
{

    use Userstamps;

    public function getModulePermissions(string $modulePrefix)
    {
        return Permission::all()->filter(function ($value, $key) use($modulePrefix) {
            return strpos($value->name,$modulePrefix) === 0;
        });
    }

    public static function getActives(): Collection{
        return LanguageModel::where('active', 1)->get();
//        return LanguageModel::active()->get();
    }

    public static function getDefault(): LanguageModel{
        return LanguageModel::default()->firstOrFail();
    }

    /**
     * Obtiene los locales de los idiomas en los que el $model tiene al menos una traducción. No la hago más específica
     * porque los distintos modelos no tienen por qué tener ningún campo en común y por eso no puedo buscar directamente
     * en el campo 'title' por ejemplo.
     * @param Model $model
     * @return array|int[]|string[]
     */
    public static function getModelLanguages(Model $model)
    {
        if (!in_array(HasTranslations::class, class_uses(get_class($model)))) {
            throw new InvalidArgumentException('El modelo debe ser traducible.');
        }

        $translations = $model->getTranslations();
        $usedLanguages = [];
        $arraysToMerge = [$usedLanguages];

        foreach (array_values($translations) as $field) {
            $arraysToMerge[] = array_keys($field);
        }
        return array_unique(array_merge(...$arraysToMerge));
    }
}
