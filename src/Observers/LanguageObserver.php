<?php

namespace Bittacora\Language\Observers;

use Bittacora\Language\Models\LanguageModel;
use Illuminate\Support\Facades\Session;

class LanguageObserver
{
    public function creating(LanguageModel $language){
        if ($language->default==1){
            $language->active=1;
        }
        return $language;
    }

    /**
     * Handle the Language "created" event.
     *
     * @param  LanguageModel  $language
     * @return void
     */
    public function created(LanguageModel $language)
    {
        if ($language->default==0){
            return;
        }

        //Ponemos todos los demás idiomas default=0
        $defaultLanguages = LanguageModel::where('default',1)->where('id','!=',$language->id)->get();
        foreach ($defaultLanguages as $otherLanguage){
            $otherLanguage->default=0;
            $otherLanguage->save();
        }
    }

    public function updating(LanguageModel $language){
        if ($language->default==1){
            $language->active=1;
        }

        if ($language->active==false AND $language->default==1){
            $language->active=true;
        }
    }

    /**
     * Handle the Language "updated" event.
     *
     * @param  LanguageModel  $language
     * @return void
     */
    public function updated(LanguageModel $language)
    {
        if ($language->default==0){
            return;
        }

        //Ponemos todos los demás idiomas default=0
        $defaultLanguages = LanguageModel::where('default',1)->where('id','!=',$language->id)->get();
        foreach ($defaultLanguages as $otherLanguage){
            $otherLanguage->default=0;
            $otherLanguage->save();
        }
    }

    public function deleting(LanguageModel $language)
    {
        if (LanguageModel::query()->count()==1 OR $language->default===1){
            return false;
        }

        return true;
    }
    /**
     * Handle the Language "deleted" event.
     *
     * @param  LanguageModel  $language
     * @return void
     */
    public function deleted(LanguageModel $language)
    {
    }

    /**
     * Handle the Language "restored" event.
     *
     * @param  LanguageModel  $language
     * @return void
     */
    public function restored(LanguageModel $language)
    {
        //
    }

    /**
     * Handle the Language "force deleted" event.
     *
     * @param  LanguageModel  $language
     * @return void
     */
    public function forceDeleted(LanguageModel $language)
    {
        //
    }
}
