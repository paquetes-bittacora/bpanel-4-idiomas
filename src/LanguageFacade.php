<?php

namespace Bittacora\Language;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Language\Language
 */
class LanguageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'language';
    }
}
