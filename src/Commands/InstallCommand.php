<?php

declare(strict_types=1);

namespace Bittacora\Language\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallCommand extends Command
{
    public $signature = 'bpanel4-language:install';

    public $description = 'Instala el módulo de idiomas de bPanel 4';

    public function handle(): void
    {
        Artisan::call('vendor:publish --tag=language --force');
        Artisan::call('db:seed --class="\\\\Bittacora\\\\Language\\\\Database\\\\Seeders\\\\DatabaseSeeder" --force');

        $path = base_path('vite.config.js');
        $originalContent = file_get_contents($path);
        if (!str_contains($originalContent, '// bittacora/bpanel4-language')) {
            $newContent = str_replace('~@bp4Start@~', "~@bp4Start@~\n// bittacora/bpanel4-language
        'resources/language/scss/language.scss',", $originalContent);
            file_put_contents($path, $newContent);
        }
    }
}
